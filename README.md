# revolute_joint_blog

Contains materials (code) from the blog __revolutejoint.blogspot.com__:

* __ros_test__ - basics: pablisher, subscriber, sevice, messages
* __scara_control__ - **ROS** package with simple model of SCARA robot for work in **Rviz** and **Gazebo** 
* __scara_moveit_config__ - autogenerated **ROS** package for SCARA control from **MoveIt!** framework 
