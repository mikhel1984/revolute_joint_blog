#!/usr/bin/env python
import rospy, actionlib
import ros_test.msg

# feedback callback
def show_feedback(msg):
   print "Current angle", msg.current_number

# main method
def angle_client():
   # simple action client
   client = actionlib.SimpleActionClient('joint_angle', ros_test.msg.AngleAction)
   client.wait_for_server()
   # set goal value
   goal = ros_test.msg.AngleGoal(count=5)
   client.send_goal(goal, feedback_cb=show_feedback)

   res = -1
   # wait for result during several seconds
   if client.wait_for_result(rospy.Duration.from_sec(10)):
      # finish before given time
      tmp = client.get_result()
      res = tmp.final_count
   else:
      # time out, cancel
      client.cancel_goal()
   return res

if __name__ == '__main__':
   try:
      rospy.init_node('joint_angle_client')   # create node
      result = angle_client()                 # run client
      print "Result is ", result              # show result
   except rospy.ROSInterruptException:
      print "Program is interrupted!"
