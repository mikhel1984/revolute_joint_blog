#!/usr/bin/env python

import rospy

def helloWorld():
   rospy.init_node('hello', anonymous=True)
   rate = rospy.Rate(1)
   while not rospy.is_shutdown():
      rospy.loginfo("Hello, World!")
      rate.sleep()

if __name__ == '__main__':
   try:
      helloWorld()
   except rospy.ROSInterruptException:
      pass
