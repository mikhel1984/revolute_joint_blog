#!/usr/bin/env python

import rospy
from std_srvs.srv import Empty

def setColor():
   rospy.init_node('bg_color', anonymous=True)
   # pring current run_id
   run_id = rospy.get_param('run_id')
   rospy.loginfo('Current run_id is ' + run_id)
   # create new parameter
   my_param = 'my_param'
   rospy.set_param(my_param, 'new_parameter')
   if rospy.has_param(my_param):
      rospy.loginfo(rospy.get_param(my_param) + ' was successfully defined')
   # update turtlesim background
   rospy.wait_for_service('clear')
   rospy.set_param('background_r', 0)
   rospy.set_param('background_g', 255)
   rospy.set_param('background_b', 0)
   clear_bg = rospy.ServiceProxy('clear',Empty)
   clear_bg()
   rospy.loginfo("Done")

if __name__ == '__main__':
   try:
      setColor()
   except rospy.ServiceException:
      pass
