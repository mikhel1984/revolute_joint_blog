#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist
from random import random

def talker():
   pub = rospy.Publisher('turtle1/cmd_vel', Twist, queue_size=10)
   rospy.init_node('publish_velocity', anonymous=True)
   rate = rospy.Rate(2)
   msg = Twist()
   while not rospy.is_shutdown():
      msg.linear.x = random()
      msg.angular.z = 2*random()-1
      pub.publish(msg)
      rate.sleep()

if __name__ == '__main__':
   try:
      talker()
   except rospy.ROSInterruptException:
      pass
