#!/usr/bin/env python

import rospy
from turtlesim.msg import Pose

def callback(data):
   rospy.loginfo("Position=({0},{1},{2})".format(data.x, data.y, data.theta))

def listener():
   rospy.init_node('subscribe_to_pose')
   rospy.Subscriber('turtle1/pose', Pose, callback)
   rospy.spin()

if __name__ == '__main__':
   listener()
