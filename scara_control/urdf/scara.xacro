<?xml version="1.0"?>
<robot name="SCARA" xmlns:xacro="http://www.ros.org/wiki/xacro">

  <!-- Parameters and constants -->
  <xacro:property name="PI" value="3.1415926535897931"/>  
  <xacro:property name="width" value="0.06" />
  <xacro:property name="len0" value="0.5" />
  <xacro:property name="len1" value="0.4" />
  <xacro:property name="len2" value="0.4" />
  <xacro:property name="len3" value="0.5" />
  <xacro:property name="ro" value="2700" />       <!-- alluminium density -->
  
  <xacro:macro name="inert" params="mass w h">
    <mass value="${mass}" />
    <inertia ixx="${mass / 12.0 * (w*w+h*h)}" ixy="0"   ixz="0"
             iyy="${mass / 12.0 * (w*w+h*h)}" iyz="0"
	     izz="${mass / 12.0 * (w*w+h*h)}" />
  </xacro:macro>
  
  <material name="blue">
    <color rgba="0 0 1 1" />
  </material>

  <material name="white">
    <color rgba="1 1 1 1" />
  </material>

  <!-- connect to world -->
  <link name="world" />
  <joint name="fixed" type="fixed">
    <parent link="world"/>
    <child link="base_link"/>
  </joint>
 
  <!-- root link -->
  <link name="base_link">
    <collision>
      <origin rpy="0 0 0" xyz="0 0 ${len0/2}" />
      <geometry>
        <cylinder length="${len0}" radius="${width/2}" />
      </geometry>
    </collision>

    <visual>
      <origin rpy="0 0 0" xyz="0 0 ${len0/2}" />
      <geometry>
        <cylinder length="${len0}" radius="${width/2}" />
      </geometry>
      <material name="white" />
    </visual>   
    
    <inertial>
      <origin rpy="0 0 0" xyz="0 0 ${len0/2}" />
      <xacro:inert mass="${ro*PI*width*width/4*len0}" w="${width}" h="${len0}" />      
    </inertial> 
  </link>

  <!-- first link -->
  <link name="link1">
    <collision>
      <geometry>
	<box size="${width} ${width} ${len1}" />
      </geometry>
      <origin rpy="0 ${PI/2} 0" xyz="${-0.5*len1} 0 0" />
    </collision>
     
    <visual>
      <geometry>
	<box size="${width} ${width} ${len1}" />
      </geometry>
      <origin rpy="0 ${PI/2} 0" xyz="${-0.5*len1} 0 0" />
      <material name="blue" />
    </visual>

    <inertial>
      <origin rpy="0 ${PI/2} 0" xyz="${-0.5*len1} 0 0" />
      <xacro:inert mass="${ro*width*width*len1}" w="${width}" h="${len1}" />      
    </inertial>
  </link>

  <joint name="base_to_link1" type="revolute">
    <axis xyz="0 0 1" />    
    <parent link="base_link" />
    <child link="link1" />
    <origin xyz="0 0 ${len0+0.5*width+0.005}" />
    <dynamics damping="0.7" />
    <limit effort="1000.0" lower="${-PI}" upper="0" velocity="0.5" />
  </joint>

  <!-- second link --> 
  <link name="link2">
    <collision>
      <geometry>
	<box size="${width} ${width} ${len2}" />
      </geometry>
      <origin rpy="0 ${0.5*PI} 0" xyz="${-0.5*len2} 0 0" />
    </collision>

    <visual>
      <geometry>
	<box size="${width} ${width} ${len2}" />
      </geometry>
      <origin rpy="0 ${0.5*PI} 0" xyz="${-0.5*len2} 0 0" />
      <material name="white" />
    </visual>

    <inertial>      
      <xacro:inert mass="${ro*width*width*len2}" w="${width}" h="${len2}" />  
      <origin rpy="0 ${0.5*PI} 0" xyz="${-0.5*len2} 0 0" />    
    </inertial>
  </link>

  <joint name="link1_to_link2" type="revolute">
    <axis xyz="0 0 1" />
    <limit effort="1000.0" lower="${-0.8*PI}" upper="${0.8*PI}" velocity="0.5" />
    <parent link="link1" />
    <child link="link2" />
    <origin xyz="${-len2} 0 ${-(width+0.005)}" />
    <dynamics damping="0.7" />
  </joint>

  <!-- third link -->  
  <link name="link3">
    <collision>
      <geometry>
        <cylinder length="${len3}" radius="${0.5*width}" />
      </geometry>
    </collision>

    <visual>
      <geometry>
        <cylinder length="${len3}" radius="${0.5*width}" />
      </geometry>
      <material name="blue" />
    </visual>

    <inertial>
      <xacro:inert mass="${ro*PI*width*width/4*len3}" w="${width}" h="${len3}" />      
    </inertial>
  </link>

  <joint name="link2_to_link3" type="prismatic">
    <axis xyz="0 0 1" />
    <limit effort="1000.0" lower="${-0.5*(len3-width)}" upper="${0.5*(len3-width)}" velocity="0.5" />
    <parent link="link2" />
    <child link="link3" />
    <origin xyz="${-(len2+width*0.5+0.005)} 0 0" />
    <dynamics damping="0.7" />
  </joint>
  
  <!-- "end effector" -->
  <link name="ee">
    <collision>
      <geometry>
        <box size="${width} ${width} ${width}" />        
      </geometry> 
      <origin rpy="0 0 0" xyz="0 0 ${-0.5*width}" />      
    </collision>
    
    <visual>
      <geometry>
        <box size="${width} ${width} ${width}" />        
      </geometry> 
      <origin rpy="0 0 0" xyz="0 0 ${-0.5*width}" />
      <material name="white" />
    </visual>
    
    <inertial>
      <xacro:inert mass="${ro*width*width*width}" w="${width}" h="${width}" /> 
      <origin rpy="0 0 0" xyz="0 0 ${-0.5*width}" />       
    </inertial>
  </link>
  
  <joint name="link3_to_ee" type="revolute">
    <axis xyz="0 0 1" />
    <limit effort="1000.0" lower="${-PI}" upper="${PI}" velocity="0.5" />
    <parent link="link3" />
    <child link="ee" />
    
    <origin xyz="0 0 ${-(0.5*len3+0.005)}" /> 
    
    <dynamics damping="0.7" />
  </joint>

  <!-- covers for link1 -->
  <link name="cover1_left">
    <visual>
      <geometry>
        <cylinder length="${width}" radius="${width*0.8}" />
      </geometry>
      <material name="blue" />
    </visual>
  </link>

  <joint name="left_cover_link1" type="fixed">
    <parent link="link1" />
    <child link="cover1_left" />
  </joint>

  <link name="cover1_right">
    <visual>
      <geometry>
        <cylinder length="${width}" radius="${width*0.8}" />
      </geometry>
      <material name="blue" />
      <origin xyz="${-len1} 0 0" />
    </visual>
  </link>

  <joint name="right_cover_link1" type="fixed">
    <parent link="link1" />
    <child link="cover1_right" />
  </joint>

  <!-- covers for link2 -->
  <link name="cover2_left">
    <visual>
      <geometry>
        <cylinder length="${width}" radius="${width*0.8}" />
      </geometry>
      <material name="white" />
    </visual>
  </link>

  <joint name="left_cover_link2" type="fixed">
    <parent link="link2" />
    <child link="cover2_left" />
  </joint>

  <link name="cover2_right">
    <visual>
      <geometry>
        <cylinder length="${width}" radius="${width*0.8}" />
      </geometry>
      <material name="white" />
      <origin xyz="${-(len2+0.5*width)} 0 0" />
    </visual>
  </link>

  <joint name="right_cover_link2" type="fixed">
    <parent link="link2" />
    <child link="cover2_right" />
  </joint>

  <!-- set transmissions -->
  <transmission name="tran1">
    <type>transmission_interface/SimpleTransmission</type>
    <joint name="base_to_link1">
      <hardwareInterface>EffortJointInterface</hardwareInterface>
    </joint>
    <actuator name="motor1">
      <hardwareInterface>EffortJointInterface</hardwareInterface>
      <mechanicalReduction>1</mechanicalReduction>
    </actuator>
  </transmission>
  
  <transmission name="tran2">
    <type>transmission_interface/SimpleTransmission</type>
    <joint name="link1_to_link2">
      <hardwareInterface>EffortJointInterface</hardwareInterface>
    </joint>
    <actuator name="motor2">
      <hardwareInterface>EffortJointInterface</hardwareInterface>
      <mechanicalReduction>1</mechanicalReduction>
    </actuator>
  </transmission>

  <transmission name="tran3">
    <type>transmission_interface/SimpleTransmission</type>
    <joint name="link2_to_link3">
      <hardwareInterface>EffortJointInterface</hardwareInterface>
    </joint>
    <actuator name="motor3">
      <hardwareInterface>EffortJointInterface</hardwareInterface>
      <mechanicalReduction>1</mechanicalReduction>
    </actuator>
  </transmission>
 
  <!-- link properties in Gazebo -->
  <gazebo reference="base_link">
    <mu1>0.2</mu1>
    <mu2>0.2</mu2>
    <material>Gazebo/White</material>
  </gazebo>
  
  <gazebo reference="link1">
    <mu1>0.2</mu1>
    <mu2>0.2</mu2>
    <material>Gazebo/Blue</material>
  </gazebo>
  
  <gazebo reference="link2">
    <mu1>0.2</mu1>
    <mu2>0.2</mu2>
    <material>Gazebo/White</material>
  </gazebo>
  
  <gazebo reference="link3">
    <mu1>0.2</mu1>
    <mu2>0.2</mu2>
    <gravity>false</gravity>
    <material>Gazebo/Blue</material>
  </gazebo>
  
  <gazebo reference="cover1_left">
    <mu1>0.2</mu1>
    <mu2>0.2</mu2>
    <material>Gazebo/Blue</material>
  </gazebo>
  
  <gazebo reference="cover1_right">
    <mu1>0.2</mu1>
    <mu2>0.2</mu2>
    <material>Gazebo/Blue</material>
  </gazebo>
  
  <gazebo reference="cover2_left">
    <mu1>0.2</mu1>
    <mu2>0.2</mu2>
    <material>Gazebo/White</material>
  </gazebo>
  
  <gazebo reference="cover2_right">
    <mu1>0.2</mu1>
    <mu2>0.2</mu2>
    <material>Gazebo/White</material>
  </gazebo>

  <!-- connect to Gazebo -->
  <gazebo>
    <plugin name="gazebo_ros_control" filename="libgazebo_ros_control.so">
       <robotNamespace>/scara</robotNamespace>
    </plugin>
  </gazebo>

</robot>
