#!/usr/bin/env python

import sys, copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

from math import pi

class MoveItScaraInterface(object):

   def __init__(self):
      "MoveIt Scara Interface"
      super(MoveItScaraInterface, self).__init__()
      # initialize
      moveit_commander.roscpp_initialize(sys.argv)
      rospy.init_node("moveit_scara_interface", anonymous=True)
      # get robot interface
      self.robot = moveit_commander.RobotCommander()
      # get world interface
      self.scene = moveit_commander.PlanningSceneInterface()
      # joint group
      self.group = moveit_commander.MoveGroupCommander("body")
      # trajectory publisher
      self.display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path',
                                                     moveit_msgs.msg.DisplayTrajectory,
						     queue_size=1)
      print "MoveIt! interface is initialized..."

   def go_to_initial_state(self):
      "Go to position in joint space"
      group = self.group
      # prepare initial values
      goal = group.get_current_joint_values()
      goal[0] = -pi/3
      goal[1] = -pi/3
      goal[2] = 0
      goal[3] = 0
      # setup
      group.go(goal, wait=True)
      # ensure that there is no residual movements
      group.stop()
      
   def go_to_pose(self):
      "Go to position in cartesian space"
      group = self.group
      # prepare position in cartesian space      
      goal = geometry_msgs.msg.Pose()      
      goal.orientation.w = 1.0         
      goal.position.x = 0.5
      goal.position.y = 0
      goal.position.z = 0.3
      # set
      group.set_pose_target(goal)
      # plan and execute     
      group.go(wait=True)      
      # ensure that there is no residual movements
      group.stop()
      # clear
      group.clear_pose_targets()
      
   def plan_cartesian_path(self, scale=1):
      "Find path"
      group = self.group
      # prepare points
      waypoints = []
      wpose = group.get_current_pose().pose
      # 1
      wpose.position.z += scale * 0.1      
      waypoints.append(copy.deepcopy(wpose))
      # 2      
      wpose.position.y = 0.6
      wpose.position.x = 0
      wpose.orientation.z += pi
      waypoints.append(copy.deepcopy(wpose))
      # 3
      wpose.position.z -= scale * 0.3
      waypoints.append(copy.deepcopy(wpose))
      # compute 
      (plan, fraction) = group.compute_cartesian_path(waypoints, 0.01, 0.0)
      # result
      return plan, fraction

   def display_trajectory(self, plan):
      "Publish trajectory for Rviz"
      robot = self.robot
      publisher = self.display_trajectory_publisher 
      # visualize
      display = moveit_msgs.msg.DisplayTrajectory()
      display.trajectory_start = robot.get_current_state()
      display.trajectory.append(plan)
      # send
      publisher.publish(display)

   def execute_plan(self, plan):
      "Move along the given path"
      group = self.group
      group.execute(plan, wait=True)
      group.stop()

def main():
   try:      
      scara = MoveItScaraInterface()

      print "Go to initial state..."
      raw_input()
      scara.go_to_initial_state() 
      
      print "Go to position in Cartesian space..."
      raw_input()
      scara.go_to_pose()
      
      print "Plan and display a Cartesian path..."
      raw_input()
      plan, fraction = scara.plan_cartesian_path()

      print "Display trajectory..."
      raw_input()
      scara.display_trajectory(plan)

      print "Execute trajectory..."
      raw_input()
      scara.execute_plan(plan)
      
      print "Bye!"
   except rospy.ROSInterruptException:
      return
   except KeyboardInterrupt:
      return

if __name__ == '__main__':
   main()
